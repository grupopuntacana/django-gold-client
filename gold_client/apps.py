from django.apps import AppConfig


class GoldClientConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gold_client'
