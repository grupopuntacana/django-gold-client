from rest_framework.authentication import BaseAuthentication
from django.contrib.auth import get_user_model
from rest_framework import exceptions
import requests
from requests.exceptions import ConnectionError
from rest_framework.exceptions import APIException
from django.conf import settings

UserModel = get_user_model()


class ServiceUnavailable(APIException):
    status_code = 503
    default_detail = 'Service temporarily unavailable, try again later.'
    default_code = 'service_unavailable'


class GoldUserAuthentication(BaseAuthentication):
    token_verify_url = settings.GOLD_TOKEN_VERIFY_URL
    timeout = 10

    def verify_user(self, token):
        try:
            response = requests.post(
                self.token_verify_url, data={'token': token},
                timeout=self.timeout)
        except ConnectionError:
            raise ServiceUnavailable()

        if response.status_code != 200:
            raise exceptions.AuthenticationFailed(
                response.json(), code=response.status_code)
        return response.json()

    def authenticate(self, request):
        headers = request.META.get('HTTP_AUTHORIZATION')
        if not headers:
            return None

        _, token = headers.split(' ')

        user_data = self.verify_user(token)

        try:
            user = UserModel.objects.get(username=user_data['username'])
        except UserModel.DoesNotExist:
            if not settings.GOLD_KEEP_USER_ID:
                user_data.pop('id')
            user = UserModel.objects.create_user(**user_data)

        if not self.user_can_authenticate(user):
            return None

        return (user, None)

    def user_can_authenticate(self, user):
        """
        Reject users with is_active=False. Custom user models that don't have
        that attribute are allowed.
        """
        is_active = getattr(user, 'is_active', None)
        return is_active or is_active is None
